# This module gives you a Shibboleth 2.x service provider.  Additional
# configuration is required for this to actually work.

# $include_apache_ssl: set to true to have this module include the Apache
# module ssl. If the calling class already does this, set this parameter
# to false to avoid duplicate resource errors.
# Default: true

class shibboleth_sp2(
  $include_apache_ssl     = true,
  $enable_newsyslog_shibd = true,
) {
  include apache

  # Install the Shibboleth packages.  Don't use stanford-shibboleth.  It's
  # not clear yet whether that will be useful at all or 2.x will make it
  # unnecessary.
  case $operatingsystem {
    'debian': {
      package { 'libapache2-mod-shib2':
        ensure  => present,
        require => $lsbdistcodename ? {
          'wheezy' => File['/etc/apt/preferences.d/shib2'],
          default   => undef
        },
      }


      # Enable the required Apache modules.
      apache::module {'shib2': ensure => present }

      if ($include_apache_ssl) {
        apache::module {'ssl': ensure => present }
      }

      case $lsbdistcodename {
         /(squeeze|wheezy)/: {
          file { '/etc/apt/preferences.d/shib2':
            source => 'puppet:///modules/shibboleth_sp2/etc/apt/preferences.d/shib2',
          }
        }
      }

      # Work around a bug with the old socket sometimes not being
      # removed even when -f is given.
      file { '/etc/init.d/shibd':
        source => 'puppet:///modules/shibboleth_sp2/etc/init.d/shibd';
      }


      # Becasue shibd does not have a proper status action on wheezy, we have
      # to make our own.
      if ($lsbdistcodename == 'wheezy') {
        file { '/usr/sbin/is-shibd-running':
          ensure => present,
          source => 'puppet:///modules/shibboleth_sp2/usr/sbin/is-shibd-running',
          mode   => '0755',
          owner  => 'root',
          group  => 'root',
        }
        service { 'shibd':
          ensure    => running,
          hasstatus => false,
          status    => '/usr/sbin/is-shibd-running',
          require => [
            Package['libapache2-mod-shib2'],
            File['/usr/sbin/is-shibd-running'],
          ]
        }
      } else {
        service { 'shibd':
          ensure  => running,
          require => Package['libapache2-mod-shib2'],
        }
      }

    } # end of case 'debian'
    'redhat': {
      package { 'shibboleth': ensure => present }
      base::rpm::import { 'shib-rpmkey':
        url       => 'http://yum.stanford.edu/RPM-GPG-KEY-SHIB',
        signature => 'gpg-pubkey-7d0a1b3d-48689588';
      }
      case $lsbdistcodename {
        'tikanga': {
          file { '/etc/yum.repos.d/shib.repo':
            source => 'puppet:///modules/shibboleth_sp2/etc/yum/shib.repo'
          }
          # Make sure the shibd daemon is running.
          service { 'shibd':
            ensure    => running,
            hasstatus => false,
            status    => 'pidof shibd',
            require   => Package['shibboleth'],
          }
        }
      }
    }
  }

  # Change the native.logger configuration, used by the Shibboleth libraries
  # embedded in Apache, to log via syslog instead of logging to files.
  file { '/etc/shibboleth/native.logger':
    source => 'puppet:///modules/shibboleth_sp2/etc/shibboleth/native.logger',
  }

  # Rotate the Shibboleth logs.
  if ($enable_newsyslog_shibd) {
    file {
      '/etc/newsyslog.daily/shibd':
        source => 'puppet:///modules/shibboleth_sp2/etc/newsyslog.daily/shibd';
      '/var/log/shibboleth/OLD':
        ensure => directory,
        mode   => 750;
    }
  }

  # Install log filtering rules.
  file { '/etc/filter-syslog/shibboleth-sp2':
    source => 'puppet:///modules/shibboleth_sp2/etc/filter-syslog/shibboleth-sp2',
  }
}
